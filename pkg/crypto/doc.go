// Package crypto provides tools for encrypting and decrypting files using
// age encryption and Shamir's Secret Sharing.
package crypto
