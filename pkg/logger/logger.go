package logger

import (
	"os"

	"github.com/sirupsen/logrus"
)

var log = logrus.New()

// init initializes the logger settings.
func init() {
	// log.SetReportCaller(true)

	log.SetOutput(os.Stdout)

	log.SetLevel(logrus.DebugLevel)
}

// GetLogger returns the configured logrus logger instance.
func GetLogger() *logrus.Logger {
	return log
}
