package storage

import (
	"bytes"
	"fmt"
	"path/filepath"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	awsS3 "github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

// S3Bucket represents an Amazon S3 bucket configuration.
type S3Bucket struct {
	Region string `mapstructure:"region"`
	Bucket string `mapstructure:"bucket"`
	Path   string `mapstructure:"path,omitempty"`
}

// Secret represents a container for secret data.
type Secret struct {
	data []byte
}

// Upload uploads data to the specified object in the Amazon S3 bucket.
func (s3 *S3Bucket) Upload(object string, data []byte) error {
	r := bytes.NewReader(data)

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(s3.Region)},
	)
	if err != nil {
		return err
	}

	if _, err := sess.Config.Credentials.Get(); err != nil {
		return err
	}

	if len(s3.Path) != 0 {
		object = filepath.ToSlash(filepath.Join(s3.Path, object))
	}

	uploader := s3manager.NewUploader(sess)

	uploadInput := &s3manager.UploadInput{
		Bucket: aws.String(s3.Bucket),
		Key:    aws.String(object),
		Body:   r,
	}

	if _, err := uploader.Upload(uploadInput); err != nil {
		return err
	}

	return nil
}

// Download downloads data from the Amazon S3 bucket with the specified prefix.
// If remove is true, it deletes the object after download.
func (s3 *S3Bucket) Download(prefix string, remove bool) ([]byte, error) {
	var object string

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(s3.Region)},
	)
	if err != nil {
		return nil, err
	}

	svc := awsS3.New(sess)

	resp, err := svc.ListObjectsV2(&awsS3.ListObjectsV2Input{Bucket: aws.String(s3.Bucket)})
	if err != nil {
		fmt.Printf("Unable to list items in bucket %q, %v", s3.Bucket, err)
	}

	if len(s3.Path) != 0 {
		prefix = filepath.ToSlash(filepath.Join(s3.Path, object))
	}

	for _, item := range resp.Contents {
		if strings.HasPrefix(*item.Key, prefix) {
			object = *item.Key
		}
	}

	downloader := s3manager.NewDownloader(sess)

	getObjectInput := &awsS3.GetObjectInput{
		Bucket: aws.String(s3.Bucket),
		Key:    aws.String(object),
	}

	secret := aws.NewWriteAtBuffer([]byte{})

	if _, err := downloader.Download(secret, getObjectInput); err != nil {
		return nil, err
	}

	if remove {
		deleteObjectInput := &awsS3.DeleteObjectInput{
			Bucket: aws.String(s3.Bucket),
			Key:    aws.String(object),
		}
		if _, err = svc.DeleteObject(deleteObjectInput); err != nil {
			return nil, err
		}
	}

	return secret.Bytes(), nil
}
