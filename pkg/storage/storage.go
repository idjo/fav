package storage

import "path/filepath"

// Storage represents a configuration for different storage types.
type Storage struct {
	Name      string    `mapstructure:"name"`
	Type      string    `mapstructure:"type"`
	S3Bucket  S3Bucket  `mapstructure:"s3,omitempty"`
	GcsBucket GcsBucket `mapstructure:"gcs,omitempty"`
}

// getBucketType determines the storage type based on the configured S3Bucket
// or GcsBucket.
func (s *Storage) getBucketType() {
	if s.S3Bucket != (S3Bucket{}) {
		s.Type = "s3"
		return
	}

	if s.GcsBucket != (GcsBucket{}) {
		s.Type = "gcs"
		return
	}
}

// Upload uploads data to the specified object in the configured cloud storage
// system.
func (s *Storage) Upload(fileName string, data []byte) error {
	s.getBucketType()

	sanitizedFileName := filepath.ToSlash(fileName)

	switch s.Type {
	case "gcs":
		if err := s.GcsBucket.Upload(sanitizedFileName, data); err != nil {
			return err
		}
	case "s3":
		if err := s.S3Bucket.Upload(sanitizedFileName, data); err != nil {
			return err
		}
	}

	return nil
}

// Download downloads data from the configured cloud storage system with the
// specified name. If remove is true, it deletes the object after download.
func (s *Storage) Download(name string, remove bool) ([]byte, error) {
	s.getBucketType()

	var secret []byte

	switch s.Type {
	case "gcs":
		b, err := s.GcsBucket.Download(name, remove)
		if err != nil {
			return nil, err
		}

		secret = b
	case "s3":
		b, err := s.S3Bucket.Download(name, remove)
		if err != nil {
			return nil, err
		}

		secret = b
	}

	return secret, nil
}
