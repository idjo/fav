package fav

import (
	"os"
	"strings"
	"runtime"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/fav-cli/fav/pkg/crypto"
	"gitlab.com/fav-cli/fav/pkg/logger"
)

var log = logger.GetLogger()

var (
	tag = "0.0.0"
	gitCommit = "unknown"
	platform = runtime.GOOS + "/" + runtime.GOARCH
)

// NewCommand creates a new cobra.Command for the "fav" CLI.
func NewCommand() *cobra.Command {
	var version string

	if len(tag) != 0 {
		version = tag	
	} else {
		version = gitCommit
	}

	c := &cobra.Command{
		Version: version + " " + platform,
		Use: "fav [-e|-d] <file>",
		Long: `fav employs age encryption for secure file encryption and decryption.
  It utilizes Shamir's Secret Sharing to split the generated age secret into
  multiple parts, distributing custody across various entities. These divided
  components are then stored in cloud storage, ensuring enhanced security and
  access control through collaborative key reconstruction.`,
		RunE: func(cmd *cobra.Command, args []string) error {
			flags := cmd.Flags()

			encrypt, err := flags.GetString("encrypt")
			if err != nil {
				return err
			}

			decrypt, err := flags.GetString("decrypt")
			if err != nil {
				return err
			}

			remove, err := flags.GetBool("remove")
			if err != nil {
				return err
			}

			if len(encrypt) != 0 || len(decrypt) != 0 {
				cwd, err := os.Getwd()
				if err != nil {
					log.Error(err)
				}

				if err := viper.ReadInConfig(); err != nil {
					log.Errorf("Config File \".fav.[yaml|yml|json|toml]\" Not Found in %s", cwd)
					os.Exit(1)
				}
			}

			if len(encrypt) != 0 {
				log.Info("encrypt mode")
				if err := crypto.Encrypt(encrypt); err != nil {
					return err
				}
			} else if len(decrypt) != 0 {
				log.Info("decrypt mode")
				if err := crypto.Decrypt(decrypt, remove); err != nil {
					return err
				}
			}

			return nil
		},
	}

	cobra.OnInitialize(initConfig)

	c.PersistentFlags().StringP("encrypt", "e", "", "encrypt a file")
	c.PersistentFlags().StringP("decrypt", "d", "", "decrypt a file")
	c.PersistentFlags().Bool("remove", true, "remove shared secret in bucket")

	c.AddCommand(
		NewCompletionCommand(),
	)

	return c
}

// initConfig initializes the configuration for the "fav" CLI.
func initConfig() {
	var log = logger.GetLogger()

	viper.AutomaticEnv()
	viper.SetEnvPrefix("fav")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))

	cwd, err := os.Getwd()
	if err != nil {
		log.Error(err)
	}

	viper.AddConfigPath(cwd)
	viper.SetConfigName(".fav")
}
