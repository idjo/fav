// Package fav provides a command-line interface (CLI) tool for secure file encryption and decryption using age encryption.
//
// Usage:
//   fav [-e|-d] <file>
//
// Commands:
//   completion [bash|zsh|fish|powershell]   Generate completion script for various shells
//
// Flags:
//   -e, --encrypt string    Encrypt a file
//   -d, --decrypt string    Decrypt a file
//       --remove            Remove shared secret in the bucket
//
// Long:
//   fav employs age encryption for secure file encryption and decryption.
//   It utilizes Shamir's Secret Sharing to split the generated age secret into
//   multiple parts, distributing custody across various entities. These divided
//   components are then stored in cloud storage, ensuring enhanced security and
//   access control through collaborative key reconstruction.
//
// Configuration:
//   The configuration for the "fav" CLI can be initialized through a
//   configuration file named ".fav.yaml" in the working directory. Supported
//   file formats for the configuration file include YAML, JSON, TOML.
//
// Example:
//   $ fav -e <file>        # Encrypt a file
//   $ fav -d <file>        # Decrypt a file
//   $ fav completion bash  # Generate Bash completion script
//
// To load completions, execute the following commands:
//   Bash:
//     $ source <(fav completion bash)
//     # To load completions for each session, execute once:
//     # Linux:
//     $ fav completion bash > /etc/bash_completion.d/fav
//     # macOS:
//     $ fav completion bash > $(brew --prefix)/etc/bash_completion.d/fav
//   Zsh:
//     $ echo "autoload -U compinit; compinit" >> ~/.zshrc
//     $ fav completion zsh > "${fpath[1]}/_fav"
//   Fish:
//     $ fav completion fish | source
//     $ fav completion fish > ~/.config/fish/completions/fav.fish
//   PowerShell:
//     PS> fav completion powershell | Out-String | Invoke-Expression
//     # To load completions for every new session, run:
//     PS> fav completion powershell > fav.ps1
//     # and source this file from your PowerShell profile.
//
// Encryption and Decryption:
//   Encryption and decryption modes are specified using the "-e" and "-d"
//   flags respectively. The tool utilizes environment variables and a
//   configuration file for additional settings. Shared secrets can be removed
//   from the bucket using the "--remove" flag.
package fav
