<p align="center">
  <img src="./assets/logo.png" width="120" height="120"/>
</p>

# fav

fav employs age encryption for secure file encryption and decryption. It
utilizes Shamir's Secret Sharing to split the generated age secret into
multiple parts, distributing custody across various entities. These divided
components are then stored in cloud storage, ensuring enhanced security and
access control through collaborative key reconstruction.

## Installation

### Go

#### Installing latest version

```
go install gitlab.com/fav-cli/fav@latest
```

#### Installing spesific version

```
go install gitlab.com/fav-cli/fav@<tag>
go install gitlab.com/fav-cli/fav@v0.0.3
```

### Build From Source

```
git clone https://gitlab.com/fav-cli/fav
cd fav
go install ./...
```

## Configuration

The configuration for the "fav" CLI can be initialized through a
configuration file named ".fav.yaml" in the working directory. Supported
file formats for the configuration file include YAML, JSON, TOML.

```yaml
storages:
  - name: ct-ember
    s3:
      region: ap-southeast-1
      bucket: ct-ember
      path: fav
  - name: idjo-ember
    gcs:
      bucket: ember.idjo.cc
      path: fav
```

The above example shows how to configure fav to use 2 storage bucket, one using
s3 and one using gcs to dual custody the generated secret key.

## Usage

### CLI

```
Usage:
  fav [-e|-d] <file> [flags]
  fav [command]

Available Commands:
  completion  Generate completion script
  help        Help about any command

Flags:
  -d, --decrypt string   decrypt a file
  -e, --encrypt string   encrypt a file
  -h, --help             help for fav
      --remove           remove shared secret in bucket

Use "fav [command] --help" for more information about a command.
```

### Library

#### Encrypt

```go
package main

import (
	"log"
	"os"

	"gitlab.com/fav-cli/fav/pkg/crypto"
)

func main() {
	tmpFile, err := os.CreateTemp("/tmp", "fav-test-file")
	if err != nil {
		log.Fatal(err)
	}
	defer tmpFile.Close()

	_, err = tmpFile.WriteString("test data")
	if err != nil {
		log.Fatal(err)
	}

	err = crypto.Encrypt(tmpFile.Name())
	if err != nil {
		log.Fatal(err)
	}
}
```

#### Decrypt

```go
package main

import (
	"log"
	"os"

	"gitlab.com/fav-cli/fav/pkg/crypto"
)

func main() {
	tmpFile, err := os.CreateTemp("/tmp", "fav-test-file")
	if err != nil {
		log.Fatal(err)
	}
	defer tmpFile.Close()

	_, err = tmpFile.WriteString("test data")
	if err != nil {
		log.Fatal(err)
	}

	err = crypto.Encrypt(tmpFile.Name())
	if err != nil {
		log.Fatal(err)
	}

	err = crypto.Decrypt(tmpFile.Name(), false)
	if err != nil {
		log.Fatal(err)
	}
}
```
